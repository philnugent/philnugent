# Phil Nugent

## Demo Projects

### Raster-Server ([source](https://gitlab.com/raster-server))
WIP: A demo project for creating a raster server for cloud-optimized geotiffs for deployment in Kubernetes. 

TODO:
- Develop ETL in Airflow for consuming and transforming raster data to sync to S3 for raster-server jobs to sync to cluster PVCs 
- Create gitlab-ci for build and deploy for raster server
- Provision OpenShift sandbox and set CI/CD to deploy
